Title: Weeknote 2023.01.30
Date: 2023-01-30
Tags: random
Category: Weeknote
Summary: 

This week was the first week in a long time I was able to get back into
the swing of things - the birth of 2nd child provides the most wonderful 
of imbalances (joy, tears and sleep) - it takes a few months to get past
the 'survival' period to reach some kind of uneasy steady state.

Technically I shut down a few more personal services as I seek to simplify
things.

Randomly ...

[Michah Dubinko](https://www.linkedin.com/search/results/content/?fromMember=%5B%22ACoAAAAPgwcB1cGdynXc1cCl8UIPKCCvEuIvrBc%22%5D&heroEntityKey=urn%3Ali%3Afsd_profile%3AACoAAAAPgwcB1cGdynXc1cCl8UIPKCCvEuIvrBc&keywords=m.joel%20dubinko&position=0&searchId=4ca51263-7ede-4e67-ae7a-0ab784d62f0e&sid=o9E&update=urn%3Ali%3Afs_updateV2%3A(urn%3Ali%3Aactivity%3A7026255577567678464%2CBLENDED_SEARCH_FEED%2CEMPTY%2CDEFAULT%2Cfalse)) wrote an interesting entry highlighting the dangers
of AI which seems related to my recent mind dribblings of AI+semantics.

I failed (miserably) to go to [FOSDEM](https://fosdem.org/2023/) again ... 
note to self - **MUST TRY HARDER**.

Wondering if its worth drafting IETF rfc on a well known URI for accessing
sbom manifests... 

