Title: Weeknote 2022.12.23
Date: 2022-12-23
Tags: random
Category: Weeknote
Summary: 

* wrote up draft of [temporal uri scheme](https://github.com/xquery/temporal-uri-scheme) rfc
* so very glad twitter is out of my life - mastodon.social is back to comms levels that was useful
* a rather smooth [curl-docker 7.87.0 release](https://hub.docker.com/r/curlimages/curl) mirroring curl release
* gearing down for the holidays
* let the emacs dotfiles refactoring begin