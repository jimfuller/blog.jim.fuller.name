Title: Leaving the bird house
Date: 2022-10-29
Tags: code
Category: Software
Summary: Leaving the bird house

For a long time, I have been chipping away at my online presence ... having been 'online'
in some form since 1990 means a lot of unintentional presence. As for the intentional presence ... twitter
is the latest focus of my attentions - it has been 14 years and its been mostly a useful tool replacing a lot
of irc type communications. I have been lucky enough (being a man and not being a minority) to have been spared 
the toxic variety of interactions so many have the misfortune of experiencing.

This is a long time coming eg. when one is the product, one knows time grows short to move on - same thing goes
for other services like google, github, linkedin or any other commercial entity whose main concern is not one's own. The
faustian bargain with these services always implies some future phase transition.

My current plan is constrained to the following:

* <del>leave twitter</del>
* leave linkedin
* leave github
* leave google (how I am going to replace SSO is a concern)

Which is the bulk of my online presence ... oddly enough it feels like a kind of downsizing but in reality it is
just a form of simplification - good at any stage in life. 

The power is always with the people (as long as they continue to have the freedom to choose) and my choice is to 
choose public spaces with decentralised technologies - naturally resistent to market or worst, nefarious forces.

I remain on [https://libera.chat](https://libera.chat) and [mastadon](https://mastodon.social/@jimfuller) (@jimfuller@mastadon.social).

_and so it goes - Twitter - son of messenger, kin of icq, illegitimate b*stard of irc inevitable descent into /dev/null_ 

