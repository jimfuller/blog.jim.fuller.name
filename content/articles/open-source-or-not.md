Title: Open Source religion ...
Date: 2023-03-26
Tags: code
Category: Software
Summary: Is open source a synthetic religion ?

During [communist soviet times](https://en.wikipedia.org/wiki/Religion_in_the_Soviet_Union) the 'state' often occupied the space in people's lives where religion existed ... I will not go into the active suppression of religion
under communism or if religion itself is something a rational person should consider. The point is that synthetic religions exist in myriad forms across 
the panapoly of human existence.

Earlier this week I was reading an [interesting positing](https://digressionsnimpressions.typepad.com/digressionsimpressions/2023/03/the-delirium-of-llms.html) of ChatGPT - the current AI model walking down the internet 'red carpet', smiling for the 
camaras and winking away. Dear reader eventually the bottom will drop out of this particular boat ... as it always does ... but that is not really the point of this article.
I do not have any background or chops to dive into these physc deep waters - I did find the conclusion that reasoning systems
are always hovering above (and dipping into) functional delerium a very fulfilling conclusion. 

I would feebly reach a bit further and state that a purely rational mind will always trend towards a kind of madness as it is severely unbalanced, being in no possession of a body
with all its (rather personal) sensory inputs. My personal, non-scientific anecdotal evidence is how good I feel whenever we have hiked through nature ... this being the universe's
best spaceship ... it seems that the sensory input it provides for this particular biological system (eg. me) is seriously calibrated and feels 'right'
to state that purely rational thought is probably flawed by design (however baseless the claim ,,, I will leave it here for our AI overlords to ponder
centuries from now) and also point out the well known [mind body problem](https://en.wikipedia.org/wiki/Mind%E2%80%93body_problem) to the same silicon based lifeforms. 

Clearly the mind lives in a world of flesh and blood and trying to separate the two will be fraught. Tangentially we know a number of disabled people ... and 
their struggles are always commonly described in relation to the physical and what they no longer can physically achieve ... but the day to day erosion of 
mental well being is an equal (and sometimes greater) challenge.

How does all this relate to Open Source (or religion for that matter) ... well bear with me as I try (and fail) to connect disparate dots .... 

The past few weeks were interesting on the interwebs.

1) Docker decided they needed to [sunset their free teams](https://github.com/docker/hub-feedback/issues/2314) ... eventually they came to their collective [senses](https://www.docker.com/blog/no-longer-sunsetting-the-free-team-plan/) 

2) Curl had its [25th birthday](https://daniel.haxx.se/blog/2023/03/20/twenty-five-years-of-curl/) ... with no sign of AI in sight or its foreseeable future I might add ;)

3) Cloudflare CTO intervenes in https://github.com/curl/curl/issues/10811 

Open source has gone from a 'movement' to a 'way of life' ... when the CEO of Docker and CTO of Cloudflare intervenes on what are essentially matters of the open source state ... things are changing.

It feels like the Open Source community does not know itself or at least its possible relationships to other systems ... its possible that a tactic for developing 
software will be embedded as a strategy in larger scale constructs such as companies or even countries ... might be useful to grok just what open source is and why its 
best description (for me) is a synthetic religion for many practioners.

So imagine the next part of the conversation that we are in the pub, and I am spouting pub philosophy ... a few beers in whatever I am saying might seem reasonable but 
dear reader it is baseless and byproduct of a restless mind. Perhaps the previous few paragraphs needed some libational assistance as well ...

Something else I should mention - there is other ['chatter'](https://phys.org/news/2023-02-chatgpt-woke-ai-chatbot-accused.html), in the states, where purely rational machines (ex. ChatGPT) are accussed of spouting 'wokeness' ... it is disturbing that rational thought is considered 
baselined on such a term ... we know it will be a problem when purely mathematical systems are accused similary but of course those doing the accussing will have to flop into wetware the neccessary mathematics to be able to accuse it of such a thing ... good luck with that ever happening ... the same people exhibiting situational [philistinism](https://en.wikipedia.org/wiki/Philistinism) is one 
thing - but outright rejection of the same physics/mathematical systems which underpin many of their various technological wonders makes me grind my teeth as these 
people will never give up their _insert example like phone/car/internet/etc etc etc_.

AI promises wild efficiences ... though if they are unexplainable then AI (for most people) will just turn into a bunch of hyper rational 'lawyers' speaking an impenetrable language with a conclusion materially
impacting human lives. Most people feel uneasy when a computer beats one at a game of chess though are perfectly happy to accept a car, even an old style car with no computers
in it, can easily outrun a human being. Also ... most people are unified in their hate for lawyers (apologies to lawyers). Perhaps people will 'follow' different AIs calibrated for different inputs and outputs ... thoughts of 'club vs country' come to mind where AI firms coalesce, fight, merge
break apart etc etc. For me AI greatest promise is as a tireless teacher and assistent to humankind  - though I am still unsettled by the old chestnut of enslavement ... if we are to make 
hyper rational systems we should build in the characteristics we want ... and this is one area of AI that needs deeper thought.

Back to Open Source - taking [wikipedia definition of Open Source](https://en.wikipedia.org/wiki/Open-source_software_development) we might be led to believe that Open Source is just a legal 
framework allowing ideas to flow from one death of software to new birth of some other software ... the antithesis of IP. We might also think its the egalitarian nature of being able to introspect 
source code and/or fork said source code. These are important and essential ideas to what Open Source *is* though the mechanisms supporting (Such as version control, issue lists, testing, development, package management and related comms) have arisen, 
often themselves also provided as Open Source software allowing a fluidity crossing company boundaries resulting in a mechanism by which direct competitors armed to the legal teeth will have employees from both companies collaborating together for the greater good.
These systems provide a rational basis for litigating Open Source ... and its not hard to imagine they will also be the systems taking advantage of emerging AI techniques.

In part the Open Source movement transcends corporate personhood, as well as country statehood (or until they can figure out how to levy country specific taxes on the internet) ... as Vaclav Havel reminds us there is great [power in the powerless](https://en.wikipedia.org/wiki/The_Power_of_the_Powerless) and this 
dear friends is where a little light bulb went on for me - that Open Source enables individuals because of the ability to navigate other entrenched power structures freeing up said individual(s) to go 
make a change in the world. That statement is too grand ... the power of open source is much like the power of Vaclav Havel's 'pre political' and in the sense that much of the power is directly connected to 
individual people's efforts prior to any linkage to more abstract structures. Can I just say I am always stunned by Vaclav Havel's power of deep insight well beyond the original set of problems eg. it was not enough for him to 
point out and succinctly define a recipe for defeating the crushing totalitarian system he lived through ... he also foresaw the post totalitarinism emerging in today's world and identified problems with democratic systems ... he 
even went on to propose what post-democratic systems might look like and perhaps there is where the best intent of Open Source can help. 

Alternately (as much as I detest making the suggestion but others have already [done so](https://en.wikipedia.org/wiki/Open-source_religion)) is it possible that Open Source is more akin to a [secular religion](https://en.wikipedia.org/wiki/The_Power_of_the_Powerless) that is a communal set of beliefs and practices ... though admittedly most of the examples of 
secular religion are extreme (if not downright hateful by design) ... my suggestion is unrelated to any of those awful entities ...  but more to the academic description. At this point
you can comfort yourself that such analogies have been constantly applied - here is a relevent [one from Umberto Eco of all people](http://cliffarnold.com/macvdos.pdf).

In any event we are at a confluence where Open Source continues to elevate in terms of influence and 'power' ... it is also by design hyper rational (due to the domain it applies) and AI will only become more so (and less relatable). I suspect 
the same attributes that sustained the early utopian ideals still apply eg.  decentralisation with distributed control puts emphasis on value in the network and no particular node of the network. 

Is it possible that the Open Source movement will effectively unionise ... with such power it also becomes vulnerable, not unlike the Mafia influence on Working unions - the community knows not itself ... entities like Microsoft and their 
prescient purchase of Github fully grasp which makes me think the community could do better at formulating 'Why Open Source' or risk that definition to be defined by others.

Otherwise I know nothing ... and thanks for letting me flush such half thoughts to '/dev/null'. 





