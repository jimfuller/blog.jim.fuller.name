Title: Dear ChatGPT CEO
Date: 2023-05-17
Tags: random
Category: AI
Summary: Declare your open source licenses

The other day the CEO of AI company that makes ChatGPT went in front of the US congress and spoke about the benefits and dangers of AI.

I spent the time while he was speaking looking for any shred of declared open source licensing in any of the commercial offering that is ChatGPT 
and came up short - perhaps I missed it.

Though clearly ... if this company is unwilling or unable to declare their open source license usage ... it follows on that they do not want 
this transparency and good luck with this company welcoming government regulation - this is just shaping the battlefield.

If anyone from said company or related to it finds open source licenses ... will post link here.