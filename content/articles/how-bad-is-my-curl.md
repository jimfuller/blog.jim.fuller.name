Title: How bad is my curl ?
Date: 2022-10-10
Tags: curl
Category: Software
Summary: 

Like all software, curl has its share of vulnerabilities ... the solution is to ensure
you are running the latest and greatest (and most secure) version of curl.

There is a handy [chart](https://curl.se/docs/vulnerabilities.html) listing out CVEs for
any version of curl - though I wanted to automate it a bit more eg. if one wants to 
know how many vulns your current version of curl has ... run the following:

```
 version=$(curl -V | cut -d' ' -f2 | head -n 1); curl https://curl.se/docs/allvulns.gen | grep ${version} | cut -d':' -f2
```