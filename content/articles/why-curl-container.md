Title: Why curlimages/curl ?
Date: 2022-10-23
Tags: curl
Category: Software
Summary: Some ruminations on why we built curlimages/curl

A recent [thread](https://twitter.com/bagder/status/1583211697925369858) by curl BDFL [Daniel Stenberg](https://twitter.com/bagder)
motivated me to write down the rationale for creating and maintaining the [official curl container](https://hub.docker.com/r/curlimages/curl).

## Security 

A few years ago there were several 'baked' curlimages/curl getting upwards of 100k-1M pulls ... these were 
static containers, already containing a number vulnerabilities. Clearly those images were going to ensure
seeding of a lot of insecure curl.

Without trying to answer 'why one would need a curl image' ... we thought if there was usage that it might
as well be usage with a secure, well tested (latest) version of curl.

The other glaring issue with most existing curl containers were that they were made for easy inheritance
which meant running curl with root user. A few years ago this was a standard practice and many questioned
why we restricted operation to a 'curl' user. It was perhaps not clear then, but it was an explicit design
decision to provide a first official version of curlimages/curl as an appliance ... in the near future we will
be providing a curl-base to use building your own images.

Often I get as confused as the next programmer wondering why things are the way they are - most of the 
confusion revolves around the fact that most technology selection is based on ease of use ... especially
in the first five minute scenario. This singular directive results in 'weird architecture' and things like
1GB web pages. 

I do not know where things will go, but I would rather that the curl project are involved.

## Kubernetes

While we cannot tell ... I can guess that a lot of curlimages/curl is set within the context of kubernetes 
- this could be as simple as status/health type checks to execution of aka 'control loop' type processing.

Something like

```
kubectl run mycurlpod --image=curlimages/curl -i --tty -- sh
```

## Old version of curl

Many package maintainers do a great job updating curl/libcurl as well as patching older versions. Often
this is not enough and users only have access to a suboptimal version of curl available. Asking that user
to use their systems package management to update curl can address the issue, but sometimes some packages
are slower to pick up the latest and greatest version of curl. Asking users to go and get the curl 
source code and build it themselves is a non starter. 

Users can just run the latest version of curl by running:

```
podman run -it curlimages/curl:latest
```

or 

```
docker run -it curlimages/curl:latest 
```

even easier if you setup an alias:

```
alias pcurl="podman run -it curlimages/curl:latest"
```

## Finally

It is easy to get the official curl docker container pull count from docker hub:

```
curl https://hub.docker.com/v2/repositories/curlimages/ | jq '.results[0].pull_count'
```

Which is nearing ~5B ... it is only a number, but I am glad most of that processing is done using
the latest version of curl ;)