Title: Temporal URI Scheme
Date: 2022-12-24
Tags: semantics
Category: Software
Summary: 

I recently wrote up draft of [temporal uri scheme](https://datatracker.ietf.org/doc/draft-temporal-uri-scheme/) rfc.

It is rather simple ... for example, given the URI: 

```
dt:20221222T162813Z
```

or

```
dt:2022-12-22T16:28:13Z 
dt:1985-04-12T23:20:50.52Z
```
Would represent a discrete point in time of "2022-12-22T16:28:13Z".

This rfc is just about the 'minting' of date-time identifiers eg. it is possible
to create two identifiers that represent that same point in time ... which 
is left for others and [OWA](https://en.wikipedia.org/wiki/Open-world_assumption)
to consider ;)

The idea is that when creating triples (in the semantic sense) often it would be useful
to define time as a subject.

```
<dt:1985-12-25T23:20:50.52Z> somens:hadEvent xmas:celebration 
```