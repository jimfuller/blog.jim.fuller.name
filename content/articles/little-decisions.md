Title: Happy New Year and small decisions...
Date: 2022-12-31
Tags: random
Category: About
Summary: Life is wonderful

In 2000 (or was it 2001???), my (now) wife and I were involved in an altercation on a night bus in 
London (going through Kentish town)... a gang of ultra violent glue sniffing youths terrorised 
the top of the double decker bus. In many respects, this evoked (some of the tamer) scenes from 
the movie 'A Clockwork Orange' - where knives are the instrument of terror.

I did something stupid and confronted them after they horribly spat upon an older couple and continued 
being terrible to everyone (the couple had the sense to leave the bus). In my mind, it was a matter
of minutes or seconds until real violence was going to erupt and I might as well choose what kind
 instead of have it imposed on us
 
The all out fracas resulted in my wife receiving a wound over her eye ...  when she was paired up 
with the one druggie female - it was an odd bit of chivalry (not the right word) that the gang allowed
that 1:1 to proceed without interference. I already knew Gabi would be my wife someday but after 
that I also knew she had a heart of a Czech Lioness. I did not fare so well, having got beat and stabbed 
in equal measure - turns out fighting ten (albeit significantly altered stated) people is not never a good idea, 
 especially when all of them were tooled up with knives - though I remained on my feet cause I knew 
 if I went to the floor that I would be getting the boot in the face treatment and that did not sound 
 like a good option.

At the time, I remembered thinking how can I get to one end of the bus and then they could only fight me 
a few at a time... not often, but sometimes (like today), I look back at that little decision one makes in 
the moment and rank that one pretty highly. Applying a 'queuing' algorithim to enforce a fighting order
allowed me to absorb their beatings piecemeal ... I am entirely certain if I was surrounded that I would
not be writing these words today.

The gang cleared off after what seemed ages getting beat by them, but the bus then continued on 
its journey, not stopping at any of the stops. Apparently the bus driver was in a bit of  
shock ... it didn't help that literally pints of my blood now decorated the top of 
the double decker bus, which was full of also shocked and dazed fellow bus passengers.

We had a dear friend with us too, who has always been a great source of solace and support and it 
was a kind of joy to know that all of us had made through something quite serious, but I was angry at 
myself for having gotten involved something that was best avoided. Since having kids I am particularly
wary of these kinds of scenarios.

The other little moment, as I was sitting there,as the bus hurtling along with everyone crying out 
for the bus to stop ... a middle aged women came up the stairs stating she was a nun and thought to 
myself that skillset is not the one I was seeking out ... turns out she was also a nurse and quickly 
applied pressure and first aid to reduce the copious bleeding. I am pretty certain her ministration 
was another example of the fine balance that life is. It is a source of regret that I tried several times 
to locate to thank her... but was never able to find her- I guess her being a nun meant she ran in 
different circles then me ;)

After what seemed to be several mins of crazy bus driving (blowing by every bus stop) and several frantic mobile phone 
calls to the police ..  a number of police cars had to do the most amazing stunt turn with multiple cars to block 
off the bus which finally forced the stunned bus driver to a halt. I am sure that none of the officers involved woke up
that morning thinking to themselves 'How to stop a runaway bus with a bleeding man on it' ... maybe they train for such
things but even I in my dazed state could appreciate the novelty of the moment.

The ambulance came and Gabs and I were able to walk off the bus, and was thinking to myself ... well that was not so 
bad but then the serious bit started, on the first step in the ambulance (knowing now that we were in good hands), I 
collapsed and completely loss conscientious due to losing 40% of one's blood.

The dream I had during that time I was out, felt like hours and hours long from my perspective (that is not 
the point of this story) - all I will say is that it was very peaceful and lovely dream.

When I awoke again we had arrived at the hospital then I promptly passed out again ... another lovely, very 
long dream ... finally woke up to Gabi who was watching many emergency nurses and doctors energectically doing 
their thing. Clearly, I was the protaganist of this story but at that very moment I felt very far away and it 
took all my effort to snap back into the same time/space dimension that my future wife occupied. 

In a matter of hours I went from near death/death hanging in a balance to feeling pretty good ...  a detective 
later mentioned the top of the bus looked like someone had slaughtered  a pig ... at the time it was 
happening I did not notice any such thing. Reality is a very strange prism indeed.

Over the decades we do not really speak about this story ... nor is it a New Years thing for me -
but from time to time I get weird pains where I was injured like today when I was holding my 2nd child (born a 
few months ago) and think to myself what a wonderful world it is to be alive. 

Wishing all a peaceful and happy New Years.



 