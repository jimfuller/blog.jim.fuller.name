Title: The laws of software release #1
Date: 2017-10-10
Tags: random
Category: Software
Summary: 

The 'Law of Software Release' asserts that any major software release will immediately be followed by a minor release within 24-48 hours.

A kind of [tunnel vision](https://en.wikipedia.org/wiki/Tachypsychia) occurs during software release where the tension between 'get it out the door' overcomes 'doing it right'.

For my own software dabblings, I tend to delay public announcement by a few days after actual release of 'bits' which seems to cover off most of the obvious issues.
