Title: Learning XQuery
Date: 2016-01-01
Tags: code
Category: Software
Summary: stale list of learning xquery

a list of resources to learn XQuery

*   curated list of [Learning XQuery resources](https://github.com/joewiz/learnxquery)
    
*   download a modern XQuery processor ([Saxon](http://www.saxonica.com/welcome/welcome.xml), [XQilla](http://xqilla.sourceforge.net/HomePage), [Zorba](http://www.zorba.io/))
    
*   download a database that supports XQuery and start learning ([MarkLogic](http://developer.marklogic.com), [exist DB](http://exist-db.org/exist/apps/homepage/index.html), [basex](http://basex.org/))
    
*   follow [@XQuery](http://twitter.com/xquery)
    
*   goto great conferences ([XML Prague](http://www.xmlprague.cz), [XML London](http://xmllondon.com), [XML Amsterdam](http://www.xmlamsterdam.com/), [XML Summer School](http://xmlsummerschool.com/), [Balisage](http://balisage.net))
    
*   [free training !](http://www.marklogic.com/training-courses/developing-marklogic-applications-i-xquery/) at MarkLogic University
    
*   [http://x-query.com/pipermail/talk/](http://x-query.com/pipermail/talk/) \[xquery mailing list\]
    
*   [XQuery at StackOverflow](http://stackoverflow.com/questions/tagged/xquery)
    
*   [Priscilla Walmsley’s excellant uptodate book 'XQuery: Search Across a Variety of XML Data'](http://www.amazon.com/XQuery-Search-Across-Variety-Data/dp/1491915102/ref=sr_1_1?s=books&ie=UTF8&qid=1453555256&sr=1-1&keywords=xquery)
    