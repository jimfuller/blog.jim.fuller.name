Title: book list
Date: 2020-02-23
Tags: random
Category: Reading
Summary: books I am reading

A random sampling of books by country which I am currently reading/read or suggested and on my list to read.

**Austria**
* "die schachnovelle" by Stefan Zweig | suggested by Hans Hübner

**Belgium**
* "Gangrene" by Jef Geraert | suggested by Robin Marx

**Germany**
* "Die neuen Leiden des jungen W.” by Ulrich Plenzdorf | suggested by Gerrit Imsieke
* "Helden wie wir" by Thomas Brussig | suggested by Gerrit Imsieke
* "Every Man Dies Alone" by Hans Fallada | suggested by Hans Hübner

**France**
* "The Plague" by Albert Camus
* "The Outsider" by Albert Camus

**Nigeria**
* "Things Fall Apart" by Chinua Achebe | Nigeria | suggested by Ihe Onwuka @mazihe

**Poland**
* "Księgi Jakubowe" by " Olga Tokarczuk" | suggested by Lech Rzedzicki

**Spain**
* "A Heart So White" by Javier Marías

**Wales**
* "One moonlit night” by Caradog Pritchard | suggested by Bethan Tovey-Walsh
