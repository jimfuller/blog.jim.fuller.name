Title: The Great Barrier reef
Date: 2022-01-02
Tags: 1990's
Category: About
Summary: 

Sometime in the 1990's we had been sailing the seas around the blue green waters 
of the Great barrier reef all day. 

We had just sheltered the 40 foot sailboat into a neat deeply hollowed 
out fjord like bay in miniature. It was paradisaical with a broad, flowing
waterfall coming off high cliffs on one side and parrot birds singing their 
songs in the trees lining the inlet. Impenetrable jungle lined the bay ... I 
wouldn't know where to start to try and step on terra firma it was so thick. 

The heat of the day was trapped here, turning us sweaty if not just a little 
bit dizzy.

As we stowed away the boat from the day of sailing, it was clear
that her father and I had not hit things off ... mainly due to
the fact he had run over a bunch of coral at the outset of our
trip. I being a sanctimonous git internally complained about the
poor coral ... I cringe now knowing the roles will reverse when
my daughter brings home someone with a similar 'cut of jib'.

I think we both silently agreed to make a good fist of things,
if not for the sake that we were all to be on a boat together
for several weeks. Lets face it he would never approve of me.  Wasn't 
to be the first or last time I encountered judgement from apprehensive
parents - I agree with them that I was hardly marriage 'timber'for
 their daughter - she was a lovely girl but I have 
no explanation why I asked for her hand in marriage during this trip. 
I do know as far as I was capable of love my proposal was pure but with all 
my failings (immature and incomplete) it would have eventually doomed 
us.

After some time in this bay, I all of sudden saw on top of the cliff 
where the waterfall ran off - a tall, slender, as far as I could 
tell amazingly gorgeous woman running in barely a bikini.

'Now what the hell is she doing here' I thought to
myself, looking around to my girlfriend, her and her twin
were looking dickensian having sensibly covered up from the 
powerful sun, wearing large brimmed straw hats. The dichotomy between them 
and this Amazon running around the jungle was so much as for I to believe all
this had been staged and as I glanced away to the twins I could see
they shared my confusion.

In an instant amazon girl jigged her running course having seen the
waterfall, splashing great big puddle strides her intent seemingly to
run straight off the cliff. At the last moment she then launched
herself into a perfect dive not worrying the least what may lie beneath
the dark waters. 

Her tanned, athletic frame whispered into the waters, where 
after an agonizingly long time she surfaced in mid-overhand stroke, swimming 
the complete distance across our little bay - disappearing to some other 
part of this uninhabited island, in the middle of a rather large ocean.

All of us on the boat were stunned silent, made even all the more dramatic when the 
captain/father informed us that this little bay of ours was one of the only 
known places in the entire world where hammerhead sharks bred and 
nurse their young.