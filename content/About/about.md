Title: About
Date: 2021-02-05

## last public presentation

Spoke about curlimages/curl at https://github.com/curl/curl-up/wiki/2022

helped setup curl teams yearly meeting in 2019 (held in Prague)
https://github.com/curl/curl-up/wiki/2019

dockercon 2020 (due to covid it was virtual)
https://www.youtube.com/watch?v=lZreoYmoMHM&list=PLkA60AVN3hh8VD2Lgyke_LwSc9BzAK4oX&ab_channel=Docker

## C++ 
simple MarkLogic client 
https://github.com/xquery/ml-utils

toy asteroid detection with openvcv
https://gitlab.com/jimfuller/sdss_asteroid_detect

curlpipe
https://gitlab.com/jimfuller/curlpipe

## java
deprecated java api integrating with semantic features
https://github.com/marklogic/marklogic-sesame

https://github.com/marklogic/java-client-api

## python
fuzzer for curl
https://gitlab.com/jimfuller/curl_fuzzy

old newrelic example
https://github.com/xquery/newrelic-plugin

## docker

did the first official marklogic docker release
https://hub.docker.com/_/marklogic

official curl docker release
https://hub.docker.com/r/curlimages/curl

## c
minor libcurl over the years
https://github.com/curl/curl

## golang
just learning golang
https://gitlab.com/jimfuller/curlbits


# communication

librechat or <a rel="me" href="https://mastodon.social/@jimfuller">Mastodon</a>

or at https://www.linkedin.com/in/jimfuller/

